
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <execinfo.h>
#include <signal.h>
#include <unistd.h>

#include "cotp.h"

#undef OPENSSL_ALGORITHM_DEFINES
#include <openssl/evp.h>
#include <openssl/hmac.h>


#define BUFSIZE 1000
#define BUF 20

void handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stdin, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

// byte_secret is unbase32 key
// byte_string is data to be HMAC'd
int hmac_algo_sha1(const char byte_secret[], const char byte_string[], char out[]) {

    // Output len
    unsigned int len = SHA1_BYTES;

    // Return the HMAC success
    return HMAC(
                EVP_sha1(),									// algorithm
                (unsigned char*)byte_secret, 10,			// key
                (unsigned char*)byte_string, 8,				// data
                (unsigned char*)out, &len) == 0 ? 0 : 1;	// output
}

int hmac_algo_sha256(const char byte_secret[], const char byte_string[], char out[]) {

    // Output len
    unsigned int len = SHA256_BYTES;

    // Return the HMAC success
    return HMAC(
                EVP_sha256(),								// algorithm
                (unsigned char*)byte_secret, 10,			// key
                (unsigned char*)byte_string, 8,				// data
                (unsigned char*)out, &len) == 0 ? 0 : 1;	// output
}

int hmac_algo_sha512(const char byte_secret[], const char byte_string[], char out[]) {

    // Output len
    unsigned int len = SHA512_BYTES;

    // Return the HMAC success
    return HMAC(
                EVP_sha512(),								// algorithm
                (unsigned char*)byte_secret, 10,			// key
                (unsigned char*)byte_string, 8,				// data
                (unsigned char*)out, &len) == 0 ? 0 : 1;	// output
}



int main(int argc, char** argv) {

    const int INTERVAL	= 30;
    const int DIGITS	= 6;
    char *a;

    // Seed random generator
    srand(time(NULL));
    // Generate random base32
    const int base32_len = 16;
    char* BASE32_SECRET = malloc(base32_len + 1 * sizeof(char));
    int key = 0;
    char ikey[BUF];
    int tv = 0;

    signal(SIGSEGV, handler);
    signal(SIGILL, handler);
    signal(SIGBUS, handler);
    signal(SIGFPE, handler);

    otp_random_base32(base32_len, otp_DEFAULT_BASE32_CHARS, BASE32_SECRET);
    BASE32_SECRET[base32_len] = '\0';



    for (int i=0; i<3; i++) {

        // Create OTPData struct, which decides the environment
        OTPData* tdata = totp_new(
                                  BASE32_SECRET,
                                  SHA1_BITS,
                                  hmac_algo_sha1,
                                  SHA1_DIGEST,
                                  DIGITS,
                                  INTERVAL);

        char buf[BUFSIZE];

        printf("Input your OTP:\n");
        fgets(buf, BUFSIZE, stdin);

        printf(buf, "\n");
        totp_verify_input(buf, ikey);
        key = strtoimax(ikey, &a, 10);
        tv = totp_verify(tdata, key, time(NULL), 4);
        printf("Verification: `%s`\n", tv == 0 ? "false" : "true");

        otp_free(tdata);
        if (tv != 0)
            execlp("/bin/sh", "/bin/sh", NULL);
    }

    exit(tv);
}

