#!/usr/bin/env sh

set -x
[ -z "$1" ] && echo "Specify username"
[ ! -f "$2" ] && echo "Specify file with passwds"

adduser --disabled-password --gecos "" $1
echo "$1:$(shuf -n1 $2)" | chpasswd
chsh -s /bin/verify.exe $1

cat >> /etc/sudoers <<< "user1   ALL=(ALL) NOPASSWD: /usr/bin/vi /etc/hosts"
